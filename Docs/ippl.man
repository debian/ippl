.\" -*-nroff-*-
.\"
.\"     Copyright (C) 1998-2000 Hugo Haas <hugo@via.ecp.fr>
.\"
.\"     This program is free software; you can redistribute it and/or modify
.\"     it under the terms of the GNU General Public License as published by
.\"     the Free Software Foundation; either version 2 of the License, or
.\"     (at your option) any later version.
.\"
.\"     This program is distributed in the hope that it will be useful,
.\"     but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"     GNU General Public License for more details.
.\"
.\"     You should have received a copy of the GNU General Public License
.\"     along with this program; if not, write to the Free Software
.\"     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
.\"
.TH IPPL 8 "Last change: 21 April 2000"

.SH NAME
ippl \- IP Protocols Logger

.SH SYNOPSIS
.B ippl
[\-hn] [-c file-name] [\-\-help] [\-\-nodaemon] [\-\-config file-name]

.SH DESCRIPTION
.I ippl
is an IP protocols logger. It logs incoming TCP connections, UDP
datagrams and ICMP packets sent to a host.
.PP
.I ippl
is based on the well-known iplogger written by Mike Edulla. The main 
drawback of iplogger is that it is not (easily) configurable.
.I ippl
has been written keeping in mind that it should be extremely
configurable and it should be easy to extend its logging capabilities.

.SH OPTIONS

.TP
.B \-c file-name, \-\-config file-name
file-name specifies an alternate configuration file to use. By
default, CONFIGURATION_FILE is used.

.TP
.B \-h, \-\-help
Print a usage message on standard output and exits successfully.

.TP
.B \-n, \-\-nodaemon
This option cause
.I ippl
not to place itself in the background. The log messages will be logged
to standard output instead of using syslog.

.SH SIGNALS

.I ippl
reacts to certain signal. An easy way to send it signals is to use the
following command:
.PP
           kill -SIGNAL `cat PID_FILE`

.TP
.B SIGHUP
This causes
.I ippl
to close all the open sockets and log files, reread the configuration
file and restart. Note that this signal should be sent to ippl if the
log files are renamed or deleted.

.TP
.B SIGTERM
.I ippl
will cleanly die.

.TP
.B SIGINT
If
.I ippl
has been started with th -n option, it will cleanly die.

.SH FILES
 __CONFIGURATION_FILE - configuration file
 /usr/share/doc/ippl/* - files worth reading if you still have a question
 __PID_FILE - file containing the PID of the running ippl

.SH SEE ALSO
ippl.conf(5), RFC768, RFC791, RFC792, RFC793, RFC1413

.SH AUTHORS
Hugo Haas (hugo@larve.net)
Etienne Bernard (eb@via.ecp.fr)
.PP
Information about ippl development can be found at
http://larve.net/ippl/.
.PP
New stable releases can be dowloaded via FTP on sunsite.unc.edu in
/pub/Linux/system/network/daemons.

.SH MAILING LISTS
Two mailing lists have been setup. Send an email to
listar@via.ecp.fr to subscribe to the announcement list
(ippl-announce) or to the development list (ippl).

.SH BUGS
If
.I ippl
spends too much time resolving host names, some packets may not be
logged.
.PP
The logclosing option logs TCP connection terminations. However, it
logs terminations initiated by both ends, which is not the expected
behavior.
.PP
Please reports any bug to ippl@via.ecp.fr
