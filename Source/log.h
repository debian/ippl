/*
 *  log.h - Logging mechanism
 *
 *  Copyright (C) 1999 Hugo Haas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef LOG_H
#define LOG_H

/** Defaults **/

/* Logging level */
#include <sys/syslog.h>
#define LOGLEVEL LOG_NOTICE

/* Structure holding info for logging */

struct loginfo {
  void (*log)(int, char *, ...);
  int level_or_fd; /* priority of the message for syslog / file descriptor */
  char *file;	/* log file to use (if any) */
  void (*open)(int *, const char *);
  void (*close)(int *, const char *);
};

void init_loginfo(struct loginfo *li);
void log_in_file(struct loginfo *li, char *filename);

/* Dummy functions */
void dummy_open(int *fd, const char *filename);
void dummy_close(int *fd, const char *filename);

void syslog_open(int *level, const char *filename);

void file_open(int *fd, const char *filename);
void file_close(int *fd, const char *filename);

#define BUFFER_SIZE 1024

void filedesc_log(int fd, char *format, ...);

#endif
