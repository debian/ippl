/*
 *  defines.h - define general constants
 *
 *  Copyright (C) 1998-1999 Hugo Haas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef DEFINES_H
#define DEFINES_H

/* General purpose declaration */
#define NONE 0
#define NOT_DEFINED 0x0000

/* IP Protocols */
#include <netinet/in.h>
#define ALL_PROTO  IPPROTO_IP

/* Boolean values */
#define TRUE  1
#define FALSE 0

/* Maximum length of a configuration file line */
#define LINE 1024

/* Size of a packet */
#define PACKET_LENGTH 8192

/* Length of a username */
#define USERNAME_LENGTH 256

/* Length of a service */
#define SERVICE_LENGTH 512

/* Length of detailed information */
#define DETAILS_LENGTH 50

/* Length of an ident reply response */
#define IDENT_LENGTH 64

/* Timeout for the ident reply */
#define IDENT_TIMEOUT 5

/* Ident port */
#define IDENT_PORT htons(113)

/* Unknown username for ident */
#define IDENT_UNKNOWN "unknown"

/* Useful types */
#ifndef _I386_TYPES_H
typedef u_int32_t __u32;
typedef u_int16_t __u16;
typedef u_int8_t __u8;
#endif

#endif
