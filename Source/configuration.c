/*
 *  configuration.c - functions parsing the configuration file
 *
 *  Copyright (C) 1998-1999 Hugo Haas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "configuration.h"
#include "filter.h"
#include "netutils.h"
#include "defines.h"
#include "log.h"

int yyparse(void);

/*
 * open_configuration
 *
 * Open the configuration file
 */

FILE *open_configuration(char *name) {
  FILE *configfile;
  extern struct loginfo log;

  configfile = fopen(name, "r");
  if (configfile == NULL) {
    log.log(log.level_or_fd, "Cannot open configuration file %s.\n", name);
    exit(1);
  }
  return configfile;
}

/*
 * parse_config_file
 * set_default_values
 *
 * Sets default values and parse the configuration file
 */

void set_default_values() {
  extern unsigned int dns_expire;
  extern unsigned short log_protocols;
  extern unsigned short resolve_protocols;
  extern unsigned short icmp_format;
  extern unsigned short tcp_format;
  extern unsigned short udp_format;
  extern unsigned short log_closing;
  extern unsigned short use_ident;
  extern unsigned int repeats;
  extern char *last_message;

  dns_expire = DNS_EXPIRE;
  log_protocols = NONE;
  resolve_protocols = 0; /* Do not resolve by default */
  icmp_format = LOGFORMAT_NORMAL;
  tcp_format = LOGFORMAT_NORMAL;
  udp_format = LOGFORMAT_NORMAL;
  log_closing = FALSE;
  use_ident = FALSE;
  repeats = 0;
  last_message = '\0';
}

void parse_config_file(char *filename) {
  FILE *configfile;
  int fd;
  extern int line_number;
  extern char *used_user;
#ifdef PARSING_DEBUG
  extern int yydebug;
#endif

#ifdef PARSING_DEBUG
  yydebug = 1;
#endif

  configfile = open_configuration(filename);
  line_number = 1;

  /* Setting default values */
  set_default_values();

  if (used_user != NULL)
    free(used_user);
  used_user = strdup(DEFAULT_USER);
  fd = fileno(configfile);
  dup2(fd, 0);
  yyparse();
}
