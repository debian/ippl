/*
 *  filter.h - Filtering mechanism
 *
 *  Copyright (C) 1998-1999 Hugo Haas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef FILTER_H
#define FILTER_H

#include "defines.h"

#define NO_ICMP_TYPE 0xff

/* Filter entry */

struct portrange_struct {
  unsigned short int range_min;
  unsigned short int range_max;
};

struct portranges_struct {
	struct portrange_struct dst;
	struct portrange_struct src;
};

struct hostdesc_struct {
  __u32 address;
  __u32 mask;
  char *hostmask;
  char *hostname;
};

union loginfo_union {
  __u8 icmptype;
  struct portranges_struct portranges;
};


struct filter_entry {
  short log;		/* TRUE for "log", FALSE for "ignore" */
  short ident;          /* TRUE if we should use ident */
  short resolve;        /* TRUE if we should resolve IP addresses */
  short logformat;      /* format used to log */
  short logclosing;     /* TRUE to log closing TCP connections */
  /* Protocol type */
  short protocol;
  /* Information about the packet */
  union loginfo_union loginfo;
  /* Mask for remote host */
  struct hostdesc_struct fromdesc;
  /* Mask for local interface */
  struct hostdesc_struct todesc;
  /* Next entry */
  struct filter_entry *next;
};

struct log_info {
  short log;
  short ident;
  short resolve;
  short logclosing;
  short logformat;
};

struct log_info do_log(const __u32 saddr, const __u32 daddr, const __u16 type, const __u16 srctype, const short protocol);

void refresh_filter(void);

void destroy_filter(void);

#endif
